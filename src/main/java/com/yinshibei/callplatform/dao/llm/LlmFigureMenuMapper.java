package com.yinshibei.callplatform.dao.llm;

import com.yinshibei.callplatform.entity.llm.LlmFigureMenu;
import com.yinshibei.callplatform.entity.llm.LlmFigureMenuExample;
import java.util.List;
import tk.mybatis.mapper.common.Mapper;

public interface LlmFigureMenuMapper extends Mapper<LlmFigureMenu> {
    List<LlmFigureMenu> selectByExample(LlmFigureMenuExample example);
}