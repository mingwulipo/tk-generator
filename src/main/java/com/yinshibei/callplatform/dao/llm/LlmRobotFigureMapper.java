package com.yinshibei.callplatform.dao.llm;

import com.yinshibei.callplatform.entity.llm.LlmRobotFigure;
import com.yinshibei.callplatform.entity.llm.LlmRobotFigureExample;
import java.util.List;
import tk.mybatis.mapper.common.Mapper;

public interface LlmRobotFigureMapper extends Mapper<LlmRobotFigure> {
    List<LlmRobotFigure> selectByExample(LlmRobotFigureExample example);
}