package com.yinshibei.callplatform.entity.llm;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Table(name = "llm_figure_menu")
public class LlmFigureMenu implements Serializable {
    /**
     * 编号
     */
    private Long id;

    /**
     * 形象设计id
     */
    @Column(name = "robot_id")
    private Long robotId;

    /**
     * 关键词
     */
    @Column(name = "key_word")
    private String keyWord;

    /**
     * 回复内容
     */
    @Column(name = "answer_content")
    private String answerContent;

    /**
     * 回复图片
     */
    @Column(name = "answer_picture")
    private String answerPicture;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 修改时间
     */
    @Column(name = "modify_time")
    private Date modifyTime;

    private static final long serialVersionUID = 1L;

    /**
     * 获取编号
     *
     * @return id - 编号
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置编号
     *
     * @param id 编号
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取形象设计id
     *
     * @return robot_id - 形象设计id
     */
    public Long getRobotId() {
        return robotId;
    }

    /**
     * 设置形象设计id
     *
     * @param robotId 形象设计id
     */
    public void setRobotId(Long robotId) {
        this.robotId = robotId;
    }

    /**
     * 获取关键词
     *
     * @return key_word - 关键词
     */
    public String getKeyWord() {
        return keyWord;
    }

    /**
     * 设置关键词
     *
     * @param keyWord 关键词
     */
    public void setKeyWord(String keyWord) {
        this.keyWord = keyWord == null ? null : keyWord.trim();
    }

    /**
     * 获取回复内容
     *
     * @return answer_content - 回复内容
     */
    public String getAnswerContent() {
        return answerContent;
    }

    /**
     * 设置回复内容
     *
     * @param answerContent 回复内容
     */
    public void setAnswerContent(String answerContent) {
        this.answerContent = answerContent == null ? null : answerContent.trim();
    }

    /**
     * 获取回复图片
     *
     * @return answer_picture - 回复图片
     */
    public String getAnswerPicture() {
        return answerPicture;
    }

    /**
     * 设置回复图片
     *
     * @param answerPicture 回复图片
     */
    public void setAnswerPicture(String answerPicture) {
        this.answerPicture = answerPicture == null ? null : answerPicture.trim();
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取修改时间
     *
     * @return modify_time - 修改时间
     */
    public Date getModifyTime() {
        return modifyTime;
    }

    /**
     * 设置修改时间
     *
     * @param modifyTime 修改时间
     */
    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }
}