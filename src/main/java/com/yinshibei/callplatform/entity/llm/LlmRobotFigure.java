package com.yinshibei.callplatform.entity.llm;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

@Table(name = "llm_robot_figure")
public class LlmRobotFigure implements Serializable {
    /**
     * 编号
     */
    private Long id;

    /**
     * 机器人ID
     */
    @Column(name = "show_id")
    private String showId;

    /**
     * 头像
     */
    @Column(name = "head_picture")
    private String headPicture;

    /**
     * 名称
     */
    private String name;

    /**
     * 简介
     */
    private String introduction;

    /**
     * 欢迎语
     */
    @Column(name = "welcome_word")
    private String welcomeWord;

    /**
     * 对话气泡
     */
    @Column(name = "converse_bubble")
    private String converseBubble;

    /**
     * 数据来源，0-仅对话训练可见，1-所有人可见
     */
    @Column(name = "document_source_status")
    private String documentSourceStatus;

    /**
     * 回答修正，0-仅对话训练可见，1-所有人可见
     */
    @Column(name = "answer_fix_status")
    private String answerFixStatus;

    /**
     * logo露出，0不显示，1显示
     */
    @Column(name = "logo_show_status")
    private String logoShowStatus;

    /**
     * logo露出内容
     */
    @Column(name = "logo_show_content")
    private String logoShowContent;

    /**
     * 删除标记，0-未删除，1-已删除
     */
    @Column(name = "delete_sign")
    private String deleteSign;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 修改时间
     */
    @Column(name = "modify_time")
    private Date modifyTime;

    private static final long serialVersionUID = 1L;

    /**
     * 获取编号
     *
     * @return id - 编号
     */
    public Long getId() {
        return id;
    }

    /**
     * 设置编号
     *
     * @param id 编号
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * 获取机器人ID
     *
     * @return show_id - 机器人ID
     */
    public String getShowId() {
        return showId;
    }

    /**
     * 设置机器人ID
     *
     * @param showId 机器人ID
     */
    public void setShowId(String showId) {
        this.showId = showId == null ? null : showId.trim();
    }

    /**
     * 获取头像
     *
     * @return head_picture - 头像
     */
    public String getHeadPicture() {
        return headPicture;
    }

    /**
     * 设置头像
     *
     * @param headPicture 头像
     */
    public void setHeadPicture(String headPicture) {
        this.headPicture = headPicture == null ? null : headPicture.trim();
    }

    /**
     * 获取名称
     *
     * @return name - 名称
     */
    public String getName() {
        return name;
    }

    /**
     * 设置名称
     *
     * @param name 名称
     */
    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    /**
     * 获取简介
     *
     * @return introduction - 简介
     */
    public String getIntroduction() {
        return introduction;
    }

    /**
     * 设置简介
     *
     * @param introduction 简介
     */
    public void setIntroduction(String introduction) {
        this.introduction = introduction == null ? null : introduction.trim();
    }

    /**
     * 获取欢迎语
     *
     * @return welcome_word - 欢迎语
     */
    public String getWelcomeWord() {
        return welcomeWord;
    }

    /**
     * 设置欢迎语
     *
     * @param welcomeWord 欢迎语
     */
    public void setWelcomeWord(String welcomeWord) {
        this.welcomeWord = welcomeWord == null ? null : welcomeWord.trim();
    }

    /**
     * 获取对话气泡
     *
     * @return converse_bubble - 对话气泡
     */
    public String getConverseBubble() {
        return converseBubble;
    }

    /**
     * 设置对话气泡
     *
     * @param converseBubble 对话气泡
     */
    public void setConverseBubble(String converseBubble) {
        this.converseBubble = converseBubble == null ? null : converseBubble.trim();
    }

    /**
     * 获取数据来源，0-仅对话训练可见，1-所有人可见
     *
     * @return document_source_status - 数据来源，0-仅对话训练可见，1-所有人可见
     */
    public String getDocumentSourceStatus() {
        return documentSourceStatus;
    }

    /**
     * 设置数据来源，0-仅对话训练可见，1-所有人可见
     *
     * @param documentSourceStatus 数据来源，0-仅对话训练可见，1-所有人可见
     */
    public void setDocumentSourceStatus(String documentSourceStatus) {
        this.documentSourceStatus = documentSourceStatus == null ? null : documentSourceStatus.trim();
    }

    /**
     * 获取回答修正，0-仅对话训练可见，1-所有人可见
     *
     * @return answer_fix_status - 回答修正，0-仅对话训练可见，1-所有人可见
     */
    public String getAnswerFixStatus() {
        return answerFixStatus;
    }

    /**
     * 设置回答修正，0-仅对话训练可见，1-所有人可见
     *
     * @param answerFixStatus 回答修正，0-仅对话训练可见，1-所有人可见
     */
    public void setAnswerFixStatus(String answerFixStatus) {
        this.answerFixStatus = answerFixStatus == null ? null : answerFixStatus.trim();
    }

    /**
     * 获取logo露出，0不显示，1显示
     *
     * @return logo_show_status - logo露出，0不显示，1显示
     */
    public String getLogoShowStatus() {
        return logoShowStatus;
    }

    /**
     * 设置logo露出，0不显示，1显示
     *
     * @param logoShowStatus logo露出，0不显示，1显示
     */
    public void setLogoShowStatus(String logoShowStatus) {
        this.logoShowStatus = logoShowStatus == null ? null : logoShowStatus.trim();
    }

    /**
     * 获取logo露出内容
     *
     * @return logo_show_content - logo露出内容
     */
    public String getLogoShowContent() {
        return logoShowContent;
    }

    /**
     * 设置logo露出内容
     *
     * @param logoShowContent logo露出内容
     */
    public void setLogoShowContent(String logoShowContent) {
        this.logoShowContent = logoShowContent == null ? null : logoShowContent.trim();
    }

    /**
     * 获取删除标记，0-未删除，1-已删除
     *
     * @return delete_sign - 删除标记，0-未删除，1-已删除
     */
    public String getDeleteSign() {
        return deleteSign;
    }

    /**
     * 设置删除标记，0-未删除，1-已删除
     *
     * @param deleteSign 删除标记，0-未删除，1-已删除
     */
    public void setDeleteSign(String deleteSign) {
        this.deleteSign = deleteSign == null ? null : deleteSign.trim();
    }

    /**
     * 获取创建时间
     *
     * @return create_time - 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 获取修改时间
     *
     * @return modify_time - 修改时间
     */
    public Date getModifyTime() {
        return modifyTime;
    }

    /**
     * 设置修改时间
     *
     * @param modifyTime 修改时间
     */
    public void setModifyTime(Date modifyTime) {
        this.modifyTime = modifyTime;
    }
}