package com.yinshibei.callplatform.entity.llm;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class LlmRobotFigureExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public LlmRobotFigureExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andShowIdIsNull() {
            addCriterion("show_id is null");
            return (Criteria) this;
        }

        public Criteria andShowIdIsNotNull() {
            addCriterion("show_id is not null");
            return (Criteria) this;
        }

        public Criteria andShowIdEqualTo(String value) {
            addCriterion("show_id =", value, "showId");
            return (Criteria) this;
        }

        public Criteria andShowIdNotEqualTo(String value) {
            addCriterion("show_id <>", value, "showId");
            return (Criteria) this;
        }

        public Criteria andShowIdGreaterThan(String value) {
            addCriterion("show_id >", value, "showId");
            return (Criteria) this;
        }

        public Criteria andShowIdGreaterThanOrEqualTo(String value) {
            addCriterion("show_id >=", value, "showId");
            return (Criteria) this;
        }

        public Criteria andShowIdLessThan(String value) {
            addCriterion("show_id <", value, "showId");
            return (Criteria) this;
        }

        public Criteria andShowIdLessThanOrEqualTo(String value) {
            addCriterion("show_id <=", value, "showId");
            return (Criteria) this;
        }

        public Criteria andShowIdLike(String value) {
            addCriterion("show_id like", value, "showId");
            return (Criteria) this;
        }

        public Criteria andShowIdNotLike(String value) {
            addCriterion("show_id not like", value, "showId");
            return (Criteria) this;
        }

        public Criteria andShowIdIn(List<String> values) {
            addCriterion("show_id in", values, "showId");
            return (Criteria) this;
        }

        public Criteria andShowIdNotIn(List<String> values) {
            addCriterion("show_id not in", values, "showId");
            return (Criteria) this;
        }

        public Criteria andShowIdBetween(String value1, String value2) {
            addCriterion("show_id between", value1, value2, "showId");
            return (Criteria) this;
        }

        public Criteria andShowIdNotBetween(String value1, String value2) {
            addCriterion("show_id not between", value1, value2, "showId");
            return (Criteria) this;
        }

        public Criteria andHeadPictureIsNull() {
            addCriterion("head_picture is null");
            return (Criteria) this;
        }

        public Criteria andHeadPictureIsNotNull() {
            addCriterion("head_picture is not null");
            return (Criteria) this;
        }

        public Criteria andHeadPictureEqualTo(String value) {
            addCriterion("head_picture =", value, "headPicture");
            return (Criteria) this;
        }

        public Criteria andHeadPictureNotEqualTo(String value) {
            addCriterion("head_picture <>", value, "headPicture");
            return (Criteria) this;
        }

        public Criteria andHeadPictureGreaterThan(String value) {
            addCriterion("head_picture >", value, "headPicture");
            return (Criteria) this;
        }

        public Criteria andHeadPictureGreaterThanOrEqualTo(String value) {
            addCriterion("head_picture >=", value, "headPicture");
            return (Criteria) this;
        }

        public Criteria andHeadPictureLessThan(String value) {
            addCriterion("head_picture <", value, "headPicture");
            return (Criteria) this;
        }

        public Criteria andHeadPictureLessThanOrEqualTo(String value) {
            addCriterion("head_picture <=", value, "headPicture");
            return (Criteria) this;
        }

        public Criteria andHeadPictureLike(String value) {
            addCriterion("head_picture like", value, "headPicture");
            return (Criteria) this;
        }

        public Criteria andHeadPictureNotLike(String value) {
            addCriterion("head_picture not like", value, "headPicture");
            return (Criteria) this;
        }

        public Criteria andHeadPictureIn(List<String> values) {
            addCriterion("head_picture in", values, "headPicture");
            return (Criteria) this;
        }

        public Criteria andHeadPictureNotIn(List<String> values) {
            addCriterion("head_picture not in", values, "headPicture");
            return (Criteria) this;
        }

        public Criteria andHeadPictureBetween(String value1, String value2) {
            addCriterion("head_picture between", value1, value2, "headPicture");
            return (Criteria) this;
        }

        public Criteria andHeadPictureNotBetween(String value1, String value2) {
            addCriterion("head_picture not between", value1, value2, "headPicture");
            return (Criteria) this;
        }

        public Criteria andNameIsNull() {
            addCriterion("name is null");
            return (Criteria) this;
        }

        public Criteria andNameIsNotNull() {
            addCriterion("name is not null");
            return (Criteria) this;
        }

        public Criteria andNameEqualTo(String value) {
            addCriterion("name =", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotEqualTo(String value) {
            addCriterion("name <>", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThan(String value) {
            addCriterion("name >", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameGreaterThanOrEqualTo(String value) {
            addCriterion("name >=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThan(String value) {
            addCriterion("name <", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLessThanOrEqualTo(String value) {
            addCriterion("name <=", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameLike(String value) {
            addCriterion("name like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotLike(String value) {
            addCriterion("name not like", value, "name");
            return (Criteria) this;
        }

        public Criteria andNameIn(List<String> values) {
            addCriterion("name in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotIn(List<String> values) {
            addCriterion("name not in", values, "name");
            return (Criteria) this;
        }

        public Criteria andNameBetween(String value1, String value2) {
            addCriterion("name between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andNameNotBetween(String value1, String value2) {
            addCriterion("name not between", value1, value2, "name");
            return (Criteria) this;
        }

        public Criteria andIntroductionIsNull() {
            addCriterion("introduction is null");
            return (Criteria) this;
        }

        public Criteria andIntroductionIsNotNull() {
            addCriterion("introduction is not null");
            return (Criteria) this;
        }

        public Criteria andIntroductionEqualTo(String value) {
            addCriterion("introduction =", value, "introduction");
            return (Criteria) this;
        }

        public Criteria andIntroductionNotEqualTo(String value) {
            addCriterion("introduction <>", value, "introduction");
            return (Criteria) this;
        }

        public Criteria andIntroductionGreaterThan(String value) {
            addCriterion("introduction >", value, "introduction");
            return (Criteria) this;
        }

        public Criteria andIntroductionGreaterThanOrEqualTo(String value) {
            addCriterion("introduction >=", value, "introduction");
            return (Criteria) this;
        }

        public Criteria andIntroductionLessThan(String value) {
            addCriterion("introduction <", value, "introduction");
            return (Criteria) this;
        }

        public Criteria andIntroductionLessThanOrEqualTo(String value) {
            addCriterion("introduction <=", value, "introduction");
            return (Criteria) this;
        }

        public Criteria andIntroductionLike(String value) {
            addCriterion("introduction like", value, "introduction");
            return (Criteria) this;
        }

        public Criteria andIntroductionNotLike(String value) {
            addCriterion("introduction not like", value, "introduction");
            return (Criteria) this;
        }

        public Criteria andIntroductionIn(List<String> values) {
            addCriterion("introduction in", values, "introduction");
            return (Criteria) this;
        }

        public Criteria andIntroductionNotIn(List<String> values) {
            addCriterion("introduction not in", values, "introduction");
            return (Criteria) this;
        }

        public Criteria andIntroductionBetween(String value1, String value2) {
            addCriterion("introduction between", value1, value2, "introduction");
            return (Criteria) this;
        }

        public Criteria andIntroductionNotBetween(String value1, String value2) {
            addCriterion("introduction not between", value1, value2, "introduction");
            return (Criteria) this;
        }

        public Criteria andWelcomeWordIsNull() {
            addCriterion("welcome_word is null");
            return (Criteria) this;
        }

        public Criteria andWelcomeWordIsNotNull() {
            addCriterion("welcome_word is not null");
            return (Criteria) this;
        }

        public Criteria andWelcomeWordEqualTo(String value) {
            addCriterion("welcome_word =", value, "welcomeWord");
            return (Criteria) this;
        }

        public Criteria andWelcomeWordNotEqualTo(String value) {
            addCriterion("welcome_word <>", value, "welcomeWord");
            return (Criteria) this;
        }

        public Criteria andWelcomeWordGreaterThan(String value) {
            addCriterion("welcome_word >", value, "welcomeWord");
            return (Criteria) this;
        }

        public Criteria andWelcomeWordGreaterThanOrEqualTo(String value) {
            addCriterion("welcome_word >=", value, "welcomeWord");
            return (Criteria) this;
        }

        public Criteria andWelcomeWordLessThan(String value) {
            addCriterion("welcome_word <", value, "welcomeWord");
            return (Criteria) this;
        }

        public Criteria andWelcomeWordLessThanOrEqualTo(String value) {
            addCriterion("welcome_word <=", value, "welcomeWord");
            return (Criteria) this;
        }

        public Criteria andWelcomeWordLike(String value) {
            addCriterion("welcome_word like", value, "welcomeWord");
            return (Criteria) this;
        }

        public Criteria andWelcomeWordNotLike(String value) {
            addCriterion("welcome_word not like", value, "welcomeWord");
            return (Criteria) this;
        }

        public Criteria andWelcomeWordIn(List<String> values) {
            addCriterion("welcome_word in", values, "welcomeWord");
            return (Criteria) this;
        }

        public Criteria andWelcomeWordNotIn(List<String> values) {
            addCriterion("welcome_word not in", values, "welcomeWord");
            return (Criteria) this;
        }

        public Criteria andWelcomeWordBetween(String value1, String value2) {
            addCriterion("welcome_word between", value1, value2, "welcomeWord");
            return (Criteria) this;
        }

        public Criteria andWelcomeWordNotBetween(String value1, String value2) {
            addCriterion("welcome_word not between", value1, value2, "welcomeWord");
            return (Criteria) this;
        }

        public Criteria andConverseBubbleIsNull() {
            addCriterion("converse_bubble is null");
            return (Criteria) this;
        }

        public Criteria andConverseBubbleIsNotNull() {
            addCriterion("converse_bubble is not null");
            return (Criteria) this;
        }

        public Criteria andConverseBubbleEqualTo(String value) {
            addCriterion("converse_bubble =", value, "converseBubble");
            return (Criteria) this;
        }

        public Criteria andConverseBubbleNotEqualTo(String value) {
            addCriterion("converse_bubble <>", value, "converseBubble");
            return (Criteria) this;
        }

        public Criteria andConverseBubbleGreaterThan(String value) {
            addCriterion("converse_bubble >", value, "converseBubble");
            return (Criteria) this;
        }

        public Criteria andConverseBubbleGreaterThanOrEqualTo(String value) {
            addCriterion("converse_bubble >=", value, "converseBubble");
            return (Criteria) this;
        }

        public Criteria andConverseBubbleLessThan(String value) {
            addCriterion("converse_bubble <", value, "converseBubble");
            return (Criteria) this;
        }

        public Criteria andConverseBubbleLessThanOrEqualTo(String value) {
            addCriterion("converse_bubble <=", value, "converseBubble");
            return (Criteria) this;
        }

        public Criteria andConverseBubbleLike(String value) {
            addCriterion("converse_bubble like", value, "converseBubble");
            return (Criteria) this;
        }

        public Criteria andConverseBubbleNotLike(String value) {
            addCriterion("converse_bubble not like", value, "converseBubble");
            return (Criteria) this;
        }

        public Criteria andConverseBubbleIn(List<String> values) {
            addCriterion("converse_bubble in", values, "converseBubble");
            return (Criteria) this;
        }

        public Criteria andConverseBubbleNotIn(List<String> values) {
            addCriterion("converse_bubble not in", values, "converseBubble");
            return (Criteria) this;
        }

        public Criteria andConverseBubbleBetween(String value1, String value2) {
            addCriterion("converse_bubble between", value1, value2, "converseBubble");
            return (Criteria) this;
        }

        public Criteria andConverseBubbleNotBetween(String value1, String value2) {
            addCriterion("converse_bubble not between", value1, value2, "converseBubble");
            return (Criteria) this;
        }

        public Criteria andDocumentSourceStatusIsNull() {
            addCriterion("document_source_status is null");
            return (Criteria) this;
        }

        public Criteria andDocumentSourceStatusIsNotNull() {
            addCriterion("document_source_status is not null");
            return (Criteria) this;
        }

        public Criteria andDocumentSourceStatusEqualTo(String value) {
            addCriterion("document_source_status =", value, "documentSourceStatus");
            return (Criteria) this;
        }

        public Criteria andDocumentSourceStatusNotEqualTo(String value) {
            addCriterion("document_source_status <>", value, "documentSourceStatus");
            return (Criteria) this;
        }

        public Criteria andDocumentSourceStatusGreaterThan(String value) {
            addCriterion("document_source_status >", value, "documentSourceStatus");
            return (Criteria) this;
        }

        public Criteria andDocumentSourceStatusGreaterThanOrEqualTo(String value) {
            addCriterion("document_source_status >=", value, "documentSourceStatus");
            return (Criteria) this;
        }

        public Criteria andDocumentSourceStatusLessThan(String value) {
            addCriterion("document_source_status <", value, "documentSourceStatus");
            return (Criteria) this;
        }

        public Criteria andDocumentSourceStatusLessThanOrEqualTo(String value) {
            addCriterion("document_source_status <=", value, "documentSourceStatus");
            return (Criteria) this;
        }

        public Criteria andDocumentSourceStatusLike(String value) {
            addCriterion("document_source_status like", value, "documentSourceStatus");
            return (Criteria) this;
        }

        public Criteria andDocumentSourceStatusNotLike(String value) {
            addCriterion("document_source_status not like", value, "documentSourceStatus");
            return (Criteria) this;
        }

        public Criteria andDocumentSourceStatusIn(List<String> values) {
            addCriterion("document_source_status in", values, "documentSourceStatus");
            return (Criteria) this;
        }

        public Criteria andDocumentSourceStatusNotIn(List<String> values) {
            addCriterion("document_source_status not in", values, "documentSourceStatus");
            return (Criteria) this;
        }

        public Criteria andDocumentSourceStatusBetween(String value1, String value2) {
            addCriterion("document_source_status between", value1, value2, "documentSourceStatus");
            return (Criteria) this;
        }

        public Criteria andDocumentSourceStatusNotBetween(String value1, String value2) {
            addCriterion("document_source_status not between", value1, value2, "documentSourceStatus");
            return (Criteria) this;
        }

        public Criteria andAnswerFixStatusIsNull() {
            addCriterion("answer_fix_status is null");
            return (Criteria) this;
        }

        public Criteria andAnswerFixStatusIsNotNull() {
            addCriterion("answer_fix_status is not null");
            return (Criteria) this;
        }

        public Criteria andAnswerFixStatusEqualTo(String value) {
            addCriterion("answer_fix_status =", value, "answerFixStatus");
            return (Criteria) this;
        }

        public Criteria andAnswerFixStatusNotEqualTo(String value) {
            addCriterion("answer_fix_status <>", value, "answerFixStatus");
            return (Criteria) this;
        }

        public Criteria andAnswerFixStatusGreaterThan(String value) {
            addCriterion("answer_fix_status >", value, "answerFixStatus");
            return (Criteria) this;
        }

        public Criteria andAnswerFixStatusGreaterThanOrEqualTo(String value) {
            addCriterion("answer_fix_status >=", value, "answerFixStatus");
            return (Criteria) this;
        }

        public Criteria andAnswerFixStatusLessThan(String value) {
            addCriterion("answer_fix_status <", value, "answerFixStatus");
            return (Criteria) this;
        }

        public Criteria andAnswerFixStatusLessThanOrEqualTo(String value) {
            addCriterion("answer_fix_status <=", value, "answerFixStatus");
            return (Criteria) this;
        }

        public Criteria andAnswerFixStatusLike(String value) {
            addCriterion("answer_fix_status like", value, "answerFixStatus");
            return (Criteria) this;
        }

        public Criteria andAnswerFixStatusNotLike(String value) {
            addCriterion("answer_fix_status not like", value, "answerFixStatus");
            return (Criteria) this;
        }

        public Criteria andAnswerFixStatusIn(List<String> values) {
            addCriterion("answer_fix_status in", values, "answerFixStatus");
            return (Criteria) this;
        }

        public Criteria andAnswerFixStatusNotIn(List<String> values) {
            addCriterion("answer_fix_status not in", values, "answerFixStatus");
            return (Criteria) this;
        }

        public Criteria andAnswerFixStatusBetween(String value1, String value2) {
            addCriterion("answer_fix_status between", value1, value2, "answerFixStatus");
            return (Criteria) this;
        }

        public Criteria andAnswerFixStatusNotBetween(String value1, String value2) {
            addCriterion("answer_fix_status not between", value1, value2, "answerFixStatus");
            return (Criteria) this;
        }

        public Criteria andLogoShowStatusIsNull() {
            addCriterion("logo_show_status is null");
            return (Criteria) this;
        }

        public Criteria andLogoShowStatusIsNotNull() {
            addCriterion("logo_show_status is not null");
            return (Criteria) this;
        }

        public Criteria andLogoShowStatusEqualTo(String value) {
            addCriterion("logo_show_status =", value, "logoShowStatus");
            return (Criteria) this;
        }

        public Criteria andLogoShowStatusNotEqualTo(String value) {
            addCriterion("logo_show_status <>", value, "logoShowStatus");
            return (Criteria) this;
        }

        public Criteria andLogoShowStatusGreaterThan(String value) {
            addCriterion("logo_show_status >", value, "logoShowStatus");
            return (Criteria) this;
        }

        public Criteria andLogoShowStatusGreaterThanOrEqualTo(String value) {
            addCriterion("logo_show_status >=", value, "logoShowStatus");
            return (Criteria) this;
        }

        public Criteria andLogoShowStatusLessThan(String value) {
            addCriterion("logo_show_status <", value, "logoShowStatus");
            return (Criteria) this;
        }

        public Criteria andLogoShowStatusLessThanOrEqualTo(String value) {
            addCriterion("logo_show_status <=", value, "logoShowStatus");
            return (Criteria) this;
        }

        public Criteria andLogoShowStatusLike(String value) {
            addCriterion("logo_show_status like", value, "logoShowStatus");
            return (Criteria) this;
        }

        public Criteria andLogoShowStatusNotLike(String value) {
            addCriterion("logo_show_status not like", value, "logoShowStatus");
            return (Criteria) this;
        }

        public Criteria andLogoShowStatusIn(List<String> values) {
            addCriterion("logo_show_status in", values, "logoShowStatus");
            return (Criteria) this;
        }

        public Criteria andLogoShowStatusNotIn(List<String> values) {
            addCriterion("logo_show_status not in", values, "logoShowStatus");
            return (Criteria) this;
        }

        public Criteria andLogoShowStatusBetween(String value1, String value2) {
            addCriterion("logo_show_status between", value1, value2, "logoShowStatus");
            return (Criteria) this;
        }

        public Criteria andLogoShowStatusNotBetween(String value1, String value2) {
            addCriterion("logo_show_status not between", value1, value2, "logoShowStatus");
            return (Criteria) this;
        }

        public Criteria andLogoShowContentIsNull() {
            addCriterion("logo_show_content is null");
            return (Criteria) this;
        }

        public Criteria andLogoShowContentIsNotNull() {
            addCriterion("logo_show_content is not null");
            return (Criteria) this;
        }

        public Criteria andLogoShowContentEqualTo(String value) {
            addCriterion("logo_show_content =", value, "logoShowContent");
            return (Criteria) this;
        }

        public Criteria andLogoShowContentNotEqualTo(String value) {
            addCriterion("logo_show_content <>", value, "logoShowContent");
            return (Criteria) this;
        }

        public Criteria andLogoShowContentGreaterThan(String value) {
            addCriterion("logo_show_content >", value, "logoShowContent");
            return (Criteria) this;
        }

        public Criteria andLogoShowContentGreaterThanOrEqualTo(String value) {
            addCriterion("logo_show_content >=", value, "logoShowContent");
            return (Criteria) this;
        }

        public Criteria andLogoShowContentLessThan(String value) {
            addCriterion("logo_show_content <", value, "logoShowContent");
            return (Criteria) this;
        }

        public Criteria andLogoShowContentLessThanOrEqualTo(String value) {
            addCriterion("logo_show_content <=", value, "logoShowContent");
            return (Criteria) this;
        }

        public Criteria andLogoShowContentLike(String value) {
            addCriterion("logo_show_content like", value, "logoShowContent");
            return (Criteria) this;
        }

        public Criteria andLogoShowContentNotLike(String value) {
            addCriterion("logo_show_content not like", value, "logoShowContent");
            return (Criteria) this;
        }

        public Criteria andLogoShowContentIn(List<String> values) {
            addCriterion("logo_show_content in", values, "logoShowContent");
            return (Criteria) this;
        }

        public Criteria andLogoShowContentNotIn(List<String> values) {
            addCriterion("logo_show_content not in", values, "logoShowContent");
            return (Criteria) this;
        }

        public Criteria andLogoShowContentBetween(String value1, String value2) {
            addCriterion("logo_show_content between", value1, value2, "logoShowContent");
            return (Criteria) this;
        }

        public Criteria andLogoShowContentNotBetween(String value1, String value2) {
            addCriterion("logo_show_content not between", value1, value2, "logoShowContent");
            return (Criteria) this;
        }

        public Criteria andDeleteSignIsNull() {
            addCriterion("delete_sign is null");
            return (Criteria) this;
        }

        public Criteria andDeleteSignIsNotNull() {
            addCriterion("delete_sign is not null");
            return (Criteria) this;
        }

        public Criteria andDeleteSignEqualTo(String value) {
            addCriterion("delete_sign =", value, "deleteSign");
            return (Criteria) this;
        }

        public Criteria andDeleteSignNotEqualTo(String value) {
            addCriterion("delete_sign <>", value, "deleteSign");
            return (Criteria) this;
        }

        public Criteria andDeleteSignGreaterThan(String value) {
            addCriterion("delete_sign >", value, "deleteSign");
            return (Criteria) this;
        }

        public Criteria andDeleteSignGreaterThanOrEqualTo(String value) {
            addCriterion("delete_sign >=", value, "deleteSign");
            return (Criteria) this;
        }

        public Criteria andDeleteSignLessThan(String value) {
            addCriterion("delete_sign <", value, "deleteSign");
            return (Criteria) this;
        }

        public Criteria andDeleteSignLessThanOrEqualTo(String value) {
            addCriterion("delete_sign <=", value, "deleteSign");
            return (Criteria) this;
        }

        public Criteria andDeleteSignLike(String value) {
            addCriterion("delete_sign like", value, "deleteSign");
            return (Criteria) this;
        }

        public Criteria andDeleteSignNotLike(String value) {
            addCriterion("delete_sign not like", value, "deleteSign");
            return (Criteria) this;
        }

        public Criteria andDeleteSignIn(List<String> values) {
            addCriterion("delete_sign in", values, "deleteSign");
            return (Criteria) this;
        }

        public Criteria andDeleteSignNotIn(List<String> values) {
            addCriterion("delete_sign not in", values, "deleteSign");
            return (Criteria) this;
        }

        public Criteria andDeleteSignBetween(String value1, String value2) {
            addCriterion("delete_sign between", value1, value2, "deleteSign");
            return (Criteria) this;
        }

        public Criteria andDeleteSignNotBetween(String value1, String value2) {
            addCriterion("delete_sign not between", value1, value2, "deleteSign");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNull() {
            addCriterion("create_time is null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIsNotNull() {
            addCriterion("create_time is not null");
            return (Criteria) this;
        }

        public Criteria andCreateTimeEqualTo(Date value) {
            addCriterion("create_time =", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotEqualTo(Date value) {
            addCriterion("create_time <>", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThan(Date value) {
            addCriterion("create_time >", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("create_time >=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThan(Date value) {
            addCriterion("create_time <", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeLessThanOrEqualTo(Date value) {
            addCriterion("create_time <=", value, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeIn(List<Date> values) {
            addCriterion("create_time in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotIn(List<Date> values) {
            addCriterion("create_time not in", values, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeBetween(Date value1, Date value2) {
            addCriterion("create_time between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andCreateTimeNotBetween(Date value1, Date value2) {
            addCriterion("create_time not between", value1, value2, "createTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeIsNull() {
            addCriterion("modify_time is null");
            return (Criteria) this;
        }

        public Criteria andModifyTimeIsNotNull() {
            addCriterion("modify_time is not null");
            return (Criteria) this;
        }

        public Criteria andModifyTimeEqualTo(Date value) {
            addCriterion("modify_time =", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeNotEqualTo(Date value) {
            addCriterion("modify_time <>", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeGreaterThan(Date value) {
            addCriterion("modify_time >", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("modify_time >=", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeLessThan(Date value) {
            addCriterion("modify_time <", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeLessThanOrEqualTo(Date value) {
            addCriterion("modify_time <=", value, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeIn(List<Date> values) {
            addCriterion("modify_time in", values, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeNotIn(List<Date> values) {
            addCriterion("modify_time not in", values, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeBetween(Date value1, Date value2) {
            addCriterion("modify_time between", value1, value2, "modifyTime");
            return (Criteria) this;
        }

        public Criteria andModifyTimeNotBetween(Date value1, Date value2) {
            addCriterion("modify_time not between", value1, value2, "modifyTime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}